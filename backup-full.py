#!/usr/bin/python
#
# Dropbox Backup Server
#
# Uses hardware:
#   Banana PI 
#   External SSD
#   USB Wifi Card
#   Custom box to keep it all orderly
#
# Designed to back up a given Dropbox directory to a local SSD
#  * Can back up from any cloud supported by rclone
#  * Uses cron to run at regular intervals
#  * Keeps daily, weekly, monthly backups via a rotation
#
#   This work is licensed under a Creative Commons
#   Attribution-NonCommercial-ShareAlike 4.0
#   International License.
#
#   Copyright 2016
#   Aaron Crandall
#   acrandal@gmail.com
#

import time
import logging
import subprocess
import os.path

BASEPATH = "/data/backup/"
BACKUPS_DIR = BASEPATH + "backups/"
SYNC_DIR = BASEPATH + "last.0/"
SYNC_TAR = BASEPATH + "last.tar.gz"
LOG_DIR = BASEPATH + "log/"
RCLONE_BIN = "/usr/sbin/rclone"


SECONDS_IN_DAY = 86300 #(low by 100 to handle wonky execution times)
SECONDS_IN_WEEK = 604800
SECONDS_IN_MONTH = 2419200

REMOTE_LOCATION = "dropbox:YOUR-DIRECTORY-PATH"

#**************************************************************#
def do_sync():
    logging.info("Synchronizing dropbox with: " + SYNC_DIR)
    try:
        result = subprocess.check_output([RCLONE_BIN, "sync", REMOTE_LOCATION, SYNC_DIR])
        logging.info(result)
    except subprocess.CalledProcessError as e:
        return_code = e.returncode
        logging.error(return_code)
        exit(1)
    except OSError as e:
        logging.error(SYNC_DIR)
        logging.error(str(e))
        exit(1)


#**************************************************************#
def do_compress():
    logging.info("Creating last.0 tarball: " + SYNC_TAR)
    try:
        result = subprocess.check_output(["tar", "czpvf", SYNC_TAR, SYNC_DIR])
        logging.info(result)
    except subprocess.CalledProcessError as e:
        return_code = e.returncode
        logging.error(return_code)
        exit(1)


#**************************************************************#
def do_rotation(name, interval, quantity):
    logging.info("Doing " + name + " rotations")
    stamp_file = "last_" + name + ".stamp"
    last_run_stamp = 0
    if os.path.exists(LOG_DIR + stamp_file):
        logging.debug("Last run file exists for " + name)
        infile = open(LOG_DIR + stamp_file)
        last_run_stamp = float((infile.readline()).strip())
        infile.close()
    else:
        logging.debug("No last run file for " + name)
    current_run_stamp = time.time()

    # Write new file to log the last run for $name
    outfile = open(LOG_DIR + stamp_file, "w")
    outfile.write(str(current_run_stamp))
    outfile.close()

    # Now do rotations if need be
    if (current_run_stamp - last_run_stamp) < interval:
        logging.debug("Recent rotation done for " + name + " - No action being taken")
    else:
        logging.debug("Doing rotation for " + name)
        curr_backups = BACKUPS_DIR + name + "."
        if os.path.exists(curr_backups + str(quantity) + ".tar.gz"):
            logging.debug("Removing oldest backup: " + curr_backups + str(quantity) + ".tar.gz")
            result = subprocess.call(["rm", "-r", curr_backups + str(quantity) + ".tar.gz"])
            if result != 0:
                logging.error("Could not delete oldest backup for " + name)
        for i in range(quantity - 1, 0, -1):
            if os.path.exists(curr_backups + str(i) + ".tar.gz"):
                logging.debug("Moving " + curr_backups + str(i) + ".tar.gz" + " to " + curr_backups + str(i+1) + ".tar.gz")
                result = subprocess.call(["mv", curr_backups + str(i) + ".tar.gz", curr_backups + str(i + 1) + ".tar.gz"])
                if result != 0:
                    logging.error("Could not move backup dir for rotation: " + curr_backups + str(i) + ".tar.gz")

        # Create new backup
        logging.debug("Creating new most recent " + name + " backup: " + curr_backups + "1" + ".tar.gz")
        result = subprocess.call(["cp", "-r", SYNC_TAR, curr_backups + "1" + ".tar.gz"])
        if result != 0:
            logging.error("Could not copy from last.0 to: " + curr_backups + "1" + ".tar.gz")


#**************************************************************#
def get_curr_hash():
    logging.debug("Hashing '" + SYNC_DIR + "' directory")
    tar = subprocess.Popen(["tar", "-cf", "-", SYNC_DIR], stdout=subprocess.PIPE)
    md5sum = subprocess.Popen(["md5sum"], stdin=tar.stdout, stdout=subprocess.PIPE)
    output = md5sum.communicate()[0]
    return(output)



#**************************************************************#
def do_rotations():
    do_rotation("daily", SECONDS_IN_DAY, 7)
    do_rotation("weekly", SECONDS_IN_WEEK, 3)
    do_rotation("monthly", SECONDS_IN_MONTH, 3)


#**************************************************************#
def main():
    logging.basicConfig(filename=LOG_DIR + "lastrun.log",
                        filemode='w',
                        level=logging.DEBUG)

    logging.info("Executing backup and rollover")

    pre_hash = get_curr_hash()
    do_sync()
    post_hash = get_curr_hash()

    #** Only run the compress and rotate if changes to data detected
    if pre_hash != post_hash:
        do_compress()
        do_rotations()


    logging.info("Done.")


if __name__ == "__main__":
    main()


